<?php
/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 02-Jun-16
 * Time: 10:24 AM
 */

class Database {

    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
        if (!isset(self::$instance)) {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            self::$instance = new PDO('mysql:host=localhost;dbname=stage_test_opdracht', 'root', '', $pdo_options);
        }
        return self::$instance;
    }
}