<?php
/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 01-Jun-16
 * Time: 9:44 PM
 */

class Controller
{
    //retrieve model and return it
    public function model($model)
    {
        require_once '../app/models/' . $model . '.php';
        return new $model();
    }

    //retrieve view and send the data to it
    public function view($view, $data = [])
    {
        require_once '../app/views/' . $view . '.php';
    }
}