<?php
/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 30-May-16
 * Time: 10:48 AM
 */


class GroupModel
{
    private $id;
    private $main_group_id;
    private $name;

    //Getters
    public function getId()
    {
        return $this->id;
    }

    public function getMainGroupId()
    {
        return $this->main_group_id;
    }

    public function getName()
    {
        return $this->name;
    }

    //Setters
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setMainGroupId($main_group_id)
    {
        $this->main_group_id = $main_group_id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public static function getGroupsForSelect()
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('SELECT * FROM groups');
            $req->setFetchMode(PDO::FETCH_CLASS, 'GroupModel');
            $req->execute();
            $groups = $req->fetchAll();
            return $groups;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function insert()
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('INSERT INTO groups(main_group_id, name) VALUES (:main_group_id, :name)');
            $req->execute(['main_group_id' => $this->main_group_id, 'name' => $this->name]);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function getMainGroups()
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('SELECT * FROM groups WHERE main_group_id is null');
            $req->setFetchMode(PDO::FETCH_CLASS, 'GroupModel');
            $req->execute();
            $groups = $req->fetchAll();
            return $groups;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $id
     */
    public function getSubGroups($id)
    {
        $db = Database::getInstance();
        $req = $db->prepare('SELECT * FROM groups WHERE main_group_id = :main_group_id');
        $req->setFetchMode(PDO::FETCH_CLASS, 'GroupModel');
        $req->execute(['main_group_id' => $id]);

        //render the subgroup and check if it has subgroups using recursion
        echo '<ul>';
        foreach($req->fetchAll() as $subGroup){
            echo '<li><a href="/stagetestopdracht/public/group/items/' . $subGroup->id . '">' . $subGroup->name . '</a></li>';

            $this->getSubGroups($subGroup->id);
        }
        echo '</ul>';
    }
}