<?php

/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 30-May-16
 * Time: 10:41 AM
 */

class ItemModel
{
    private $id;
    private $group_id;
    private $name;


    //Getters
    public function getId()
    {
        return $this->id;
    }

    public function getGroupId()
    {
        return $this->group_id;
    }

    public function getName()
    {
        return $this->name;
    }

    //Setters
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $id
     */
    public function getItem($id)
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('SELECT * FROM items WHERE id = :id');
            $req->execute(array('id' => $id));
            $item = $req->fetch();
            $this->setId($item['id']);
            $this->setName($item['name']);
            $this->setGroupId($item['group_id']);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public static function getItems()
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('SELECT * FROM items');
            $req->setFetchMode(PDO::FETCH_CLASS, 'ItemModel');
            $req->execute();
            $items = $req->fetchAll();
            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function insert()
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('INSERT INTO items(group_id, name) VALUES (:group_id, :name)');
            $req->execute(['group_id' => $this->group_id, 'name' => $this->name]);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return array
     */
    public static function getGroupItems($id)
    {
        try {
            $db = Database::getInstance();
            $req = $db->prepare('SELECT * FROM items WHERE group_id = :group_id');
            $req->setFetchMode(PDO::FETCH_CLASS, 'ItemModel');
            $req->execute(['group_id' => $id]);
            $items = $req->fetchAll();
            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}