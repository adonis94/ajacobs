
<form action="" method="post">
    <div>
        <label for="group">Group: </label>
        <select name="group" id="group">

            <?php foreach($data['groups'] as $group) { ?>
                <option value="<?=$group->getId()?>"> <?=$group->getName()?></option>

            <?php } ?>
        </select>
    </div>
    <div>
        <label for="name">Name: </label>
        <input type="text" name="name" id="name"/>
    </div>
    <div>
        <input type="submit" name="submit" value="Save" />
    </div>
</form>


Go to: <br>
<a href="/stagetestopdracht/public/item/all/">Items Overview </a> &nbsp;&nbsp;&nbsp;&nbsp;
<a href="/stagetestopdracht/public/group/add/">Add Group </a>
