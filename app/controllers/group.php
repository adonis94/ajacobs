<?php
/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 02-Jun-16
 * Time: 9:54 AM
 */

class Group extends Controller
{
    public function overview()
    {
        $group = $this->model('GroupModel');
        $groups = $group->getMainGroups();
        $this->view('group/overview', ['groups' => $groups]);
    }

    public function add()
    {
        $group = $this->model('GroupModel');
        if(isset($_POST['submit'])){

            $group->setMainGroupId($_POST['main_group'] != 0 ? $_POST['main_group'] : null);
            $group->setName($_POST['name']);
            $group->insert();
        }

        $groups = $group->getGroupsForSelect();
        $this->view('group/add', ['groups' => $groups]);
    }

    public function items($id = '')
    {
        $item = $this->model('ItemModel');
        $items = $item->getGroupItems($id);
        $this->view('group/items', ['items' => $items]);

    }
}