<?php
/**
 * Created by PhpStorm.
 * User: Adonis
 * Date: 02-Jun-16
 * Time: 9:53 AM
 */

Class Item extends Controller
{
    public function all()
    {
        $item = $this->model('ItemModel');
        $items = $item->getItems();
        $this->view('item/all', ['items' => $items]);
    }

    public function add()
    {
        if(isset($_POST['submit'])){

            $item = $this->model('ItemModel');
            $item->setGroupId($_POST['group']);
            $item->setName($_POST['name']);
            $item->insert();
        }

        $group = $this->model('GroupModel');
        $groups = $group->getGroupsForSelect();
        $this->view('item/add', ['groups' => $groups]);

    }

    public function details($id = '')
    {
        $item = $this->model('ItemModel');
        $item->getItem($id);
        $this->view('item/details', ['name' => $item->getName(), 'group_id' => $item->getGroupId(), 'id' => $item->getId()]);
    }
}